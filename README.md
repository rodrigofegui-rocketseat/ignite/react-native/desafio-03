# Desafio 03 - Componentes e API

Projeto destinado na resolução do **Desafio 03 - Componentes e API: Marketspace**

![Desafio elaborado pela Rocketseat](/docs/images/challengue_cover.png)

Conforme ilustrado na imagem acima, trata-se de uma aplicação de anúncios de produtos, que disponilizará as seguintes funcionalidades:

- Login e cadastro de usuários
- Gerenciamento de produtos
- Listagem de produtos com busca e filtros
- Envio de múltiplas imagens
- E várias outras...

A API a ser consumida por esta aplicação foi desenvolvida pela equipe da Rocketseat, sendo disponibilizada via repositório.
O mesmo valendo para a interface e a experiência do usuário, que foram via projeto Figma. Tudo isso servirá de base para
o desenvolvimento deste projeto, almejando a implementação de tudo que lá foi estipulado, salvo limitações do próprio desenvolvedor.

## Dependências

- Aparentemente o `native-base`, utilizado neste projeto, não possui mais suporte ativo. Não foi investigado uma
substituição até o presente momento. Sendo necessário alterar o código do mesmo conforme
[este comentário, da Issue #5758 no NativeBase](https://github.com/GeekyAnts/NativeBase/issues/5758#issuecomment-1732501711)
