import { AnimationResolver } from '@gluestack-style/animation-resolver';
import { MotionAnimationDriver } from '@gluestack-style/legend-motion-animation-driver';
import { createComponents, createConfig } from '@gluestack-style/react';

import * as componentsTheme from './theme';
import { APP_TOKENS } from './tokens';

export const gluestackUIConfig = createConfig({
    aliases: {
        bg: 'backgroundColor',
        bgColor: 'backgroundColor',
        h: 'height',
        w: 'width',
        p: 'padding',
        px: 'paddingHorizontal',
        py: 'paddingVertical',
        pt: 'paddingTop',
        pb: 'paddingBottom',
        pr: 'paddingRight',
        pl: 'paddingLeft',
        m: 'margin',
        mx: 'marginHorizontal',
        my: 'marginVertical',
        mt: 'marginTop',
        mb: 'marginBottom',
        mr: 'marginRight',
        ml: 'marginLeft',
        rounded: 'borderRadius',
    } as const,
    tokens: APP_TOKENS,
    globalStyle: {
        variants: {
            hardShadow: {
                '1': {
                    shadowColor: '$backgroundLight900',
                    shadowOffset: {
                        width: -2,
                        height: 2,
                    },
                    shadowRadius: 8,
                    shadowOpacity: 0.5,
                    elevation: 10,
                },
                '2': {
                    shadowColor: '$backgroundLight900',
                    shadowOffset: {
                        width: 0,
                        height: 3,
                    },
                    shadowRadius: 8,
                    shadowOpacity: 0.5,
                    elevation: 10,
                },
                '3': {
                    shadowColor: '$backgroundLight900',
                    shadowOffset: {
                        width: 2,
                        height: 2,
                    },
                    shadowRadius: 8,
                    shadowOpacity: 0.5,
                    elevation: 10,
                },
                '4': {
                    shadowColor: '$backgroundLight900',
                    shadowOffset: {
                        width: 0,
                        height: -3,
                    },
                    shadowRadius: 8,
                    shadowOpacity: 0.5,
                    elevation: 10,
                },
                // this 5th version is only for toast shadow
                // temporary
                '5': {
                    shadowColor: '$backgroundLight900',
                    shadowOffset: {
                        width: 0,
                        height: 3,
                    },
                    shadowRadius: 8,
                    shadowOpacity: 0.2,
                    elevation: 10,
                },
            },
            softShadow: {
                '1': {
                    shadowColor: '$backgroundLight900',
                    shadowOffset: {
                        width: 0,
                        height: 0,
                    },
                    shadowRadius: 10,
                    shadowOpacity: 0.1,
                    _android: {
                        shadowColor: '$backgroundLight500',
                        elevation: 5,
                        shadowOpacity: 0.05,
                    },
                },
                '2': {
                    shadowColor: '$backgroundLight900',
                    shadowOffset: {
                        width: 0,
                        height: 0,
                    },
                    shadowRadius: 20,
                    elevation: 3,
                    shadowOpacity: 0.1,
                    _android: {
                        shadowColor: '$backgroundLight500',
                        elevation: 10,
                        shadowOpacity: 0.1,
                    },
                },
                '3': {
                    shadowColor: '$backgroundLight900',
                    shadowOffset: {
                        width: 0,
                        height: 0,
                    },
                    shadowRadius: 30,
                    shadowOpacity: 0.1,
                    elevation: 4,
                    _android: {
                        shadowColor: '$backgroundLight500',
                        elevation: 15,
                        shadowOpacity: 0.15,
                    },
                },
                '4': {
                    shadowColor: '$backgroundLight900',
                    shadowOffset: {
                        width: 0,
                        height: 0,
                    },
                    shadowRadius: 40,
                    shadowOpacity: 0.1,
                    elevation: 10,
                    _android: {
                        shadowColor: '$backgroundLight500',
                        elevation: 20,
                        shadowOpacity: 0.2,
                    },
                },
            },
        },
    },
    plugins: [new AnimationResolver(MotionAnimationDriver)],
});

export const componentsConfig = createComponents(componentsTheme);

export type { UIComponents, UIConfig } from '@gluestack-ui/themed';

export const config = {
    ...gluestackUIConfig,
    components: componentsConfig,
};
