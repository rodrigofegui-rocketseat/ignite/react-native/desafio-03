const COMMON_TOKENS = {
    colors: {
        gray100: '#F7F7F8',
        gray200: '#EDECEE',
        gray300: '#D9D8DA',
        gray400: '#9F9BA1',
        gray500: '#5F5B62',
        gray600: '#3E3A40',
        gray700: '#1A181B',

        blue600: '#364D9D',
        blue400: '#647AC7',

        red400: '#EE7979',
    },
    space: {
        '5.5': 22,
        '10.5': 42,
        '18': 72,
        '22': 88,
        '26.5': 106,
    },
    borderWidths: {
        '3': 3,
    },
    fonts: {
        heading: 'Karla_700Bold',
        body: 'Karla_400Regular',
        light: 'Karla_300Light',
    },
    lineHeights: {
        '130%': 1.3,
    },
};

export const CUSTOM_TOKENS = {
    ...COMMON_TOKENS,
    colors: {
        ...COMMON_TOKENS.colors,
        secondary400: COMMON_TOKENS.colors.red400,

        primary600: COMMON_TOKENS.colors.blue600,
        primary400: COMMON_TOKENS.colors.blue400,
    },
};
