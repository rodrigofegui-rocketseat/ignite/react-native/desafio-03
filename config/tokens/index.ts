import { CUSTOM_TOKENS } from './custom';
import { DEFAULT_TOKENS } from './default';

export const APP_TOKENS = {
    ...DEFAULT_TOKENS,
    colors: {
        ...DEFAULT_TOKENS.colors,
        ...CUSTOM_TOKENS.colors,
    },
    space: {
        ...DEFAULT_TOKENS.space,
        ...CUSTOM_TOKENS.space,
    },
    borderWidths: {
        ...DEFAULT_TOKENS.borderWidths,
        ...CUSTOM_TOKENS.borderWidths,
    },
    fonts: {
        ...DEFAULT_TOKENS.fonts,
        ...CUSTOM_TOKENS.fonts,
    },
    lineHeights: {
        ...DEFAULT_TOKENS.lineHeights,
        ...CUSTOM_TOKENS.lineHeights,
    },
};
