import {
    componentsConfig,
    config,
    gluestackUIConfig,
} from '@config/gluestack-ui.config';

type ConfigType = typeof config;

type Config = typeof gluestackUIConfig; // Assuming `config` is defined elsewhere

type Components = typeof componentsConfig;

export interface IConfig {}
export interface IComponents {}

declare module '@gluestack-style/react' {
    interface ICustomConfig extends ConfigType {}
}

declare module '@gluestack-ui/themed' {
    interface UIConfig extends Omit<Config, keyof IConfig>, IConfig {}
    interface UIComponents
        extends Omit<Components, keyof IComponents>,
            IComponents {}
}
