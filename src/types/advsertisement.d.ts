import { UserData } from './user';

export type Advertisement = {
    id: string;
    user: UserData;
    imageUrls: string[];
    title: string;
    price: number;
    isNew: boolean;
    isActive: boolean;
};
