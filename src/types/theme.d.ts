import { APP_TOKENS } from '@config/tokens';

export type Fonts = keyof typeof APP_TOKENS.fonts;

export type Colors = keyof typeof APP_TOKENS.colors;

export type FontWeights = keyof typeof APP_TOKENS.fontWeights;

export type FontSizes = keyof typeof APP_TOKENS.fontSizes;

export type Spaces = keyof typeof APP_TOKENS.space;

export type BorderWidths = keyof typeof APP_TOKENS.borderWidths;
