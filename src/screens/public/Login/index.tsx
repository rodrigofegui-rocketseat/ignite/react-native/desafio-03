import { MSScreenView } from '@app/components/MSScreenView';

import { Footer } from './components/Footer';
import { Form } from './components/Form';
import { Logo } from './components/Logo';
import { Panel } from './components/Panel';

export const Login = () => {
    return (
        <MSScreenView bgColor='$gray200'>
            <Panel>
                <Logo />

                <Form />
            </Panel>

            <Footer />
        </MSScreenView>
    );
};
