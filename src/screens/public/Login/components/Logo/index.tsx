import { Center } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSLogo } from '@app/components/MSLogo';
import { MSText } from '@app/components/MSText';

export const Logo: FC = () => {
    return (
        <Center my='$16'>
            <MSLogo mb='$5' />
            <MSText
                tipo='título'
                fontSize='$4xl'>
                marketspace
            </MSText>
            <MSText
                tipo='subtítulo'
                fontSize='$sm'>
                Seu espaço de compra e venda
            </MSText>
        </Center>
    );
};
