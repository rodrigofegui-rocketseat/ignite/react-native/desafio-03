import { Center, Box } from '@gluestack-ui/themed';
import { yupResolver } from '@hookform/resolvers/yup';
import { FC, useRef } from 'react';
import { Controller, useForm } from 'react-hook-form';

import { MSButton } from '@app/components/MSButton';
import { MSInput } from '@app/components/MSInput';
import { SignableTextInputRef } from '@app/components/MSInput/TextInput/Root/types';
import { MSText } from '@app/components/MSText';
import { useInsets } from '@app/hooks/useInsets';
import { autoFocusOn } from '@app/utils/references';

import { handleSignIn } from './controller';
import { loginSchema } from './schemas';
import { LoginFormData } from './types';

export const Form: FC = () => {
    const passwordRef = useRef<SignableTextInputRef | null>(null);
    const { left } = useInsets();
    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm<LoginFormData>({
        resolver: yupResolver(loginSchema),
    });

    return (
        <Center px={2 * left}>
            <MSText>Acesse sua conta</MSText>

            <Controller
                control={control}
                name='email'
                render={({ field: { onChange, value } }) => (
                    <MSInput.TextInput.Root
                        type='text'
                        placeholder='E-mail'
                        keyboardType='email-address'
                        returnKeyType='next'
                        value={value}
                        onChangeText={onChange}
                        errorMessage={errors.email?.message}
                        onSubmitEditing={() =>
                            autoFocusOn<SignableTextInputRef>(passwordRef)
                        }
                    />
                )}
            />

            <Controller
                control={control}
                name='password'
                render={({ field: { onChange, value } }) => (
                    <MSInput.TextInput.Root
                        ref={passwordRef}
                        type='password'
                        placeholder='Senha'
                        returnKeyType='done'
                        value={value}
                        onChangeText={onChange}
                        onSubmitEditing={handleSubmit(handleSignIn)}
                        errorMessage={errors.password?.message}
                    />
                )}
            />

            <Box
                mt='$8'
                w='$full'>
                <MSButton.Root
                    variant='primary'
                    onPress={handleSubmit(handleSignIn)}>
                    <MSButton.Text>Entrar</MSButton.Text>
                </MSButton.Root>
            </Box>
        </Center>
    );
};
