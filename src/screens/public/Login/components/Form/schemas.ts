import { object, string } from 'yup';

export const loginSchema = object({
    email: string().required('Informe o e-mail.').email('E-mail inválido.'),
    password: string().required('Informe a senha.'),
});
