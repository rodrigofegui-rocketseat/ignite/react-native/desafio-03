import { VStack } from '@gluestack-ui/themed';
import { FC, PropsWithChildren } from 'react';

import { useInsets } from '@app/hooks/useInsets';

export const Panel: FC<PropsWithChildren> = ({ children }) => {
    const { top, left, right } = useInsets();

    return (
        <VStack
            flex={1}
            bg='$gray200'
            mt={-top}
            ml={-left}
            mr={-right}
            borderBottomLeftRadius='$3xl'
            borderBottomRightRadius='$3xl'>
            {children}
        </VStack>
    );
};
