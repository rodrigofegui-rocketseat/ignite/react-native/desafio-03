import { Box, Center } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSButton } from '@app/components/MSButton';
import { MSText } from '@app/components/MSText';
import { useInsets } from '@app/hooks/useInsets';

export const Footer: FC = () => {
    const { left } = useInsets();

    return (
        <Center
            py='$12'
            px={left}>
            <MSText mb='$4'>Ainda não tem acesso?</MSText>

            <Box w='$full'>
                <MSButton.Root variant='secondary'>
                    <MSButton.Text>Criar uma conta</MSButton.Text>
                </MSButton.Root>
            </Box>
        </Center>
    );
};
