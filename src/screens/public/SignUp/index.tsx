import { FC } from 'react';

import { MSScreenView } from '@app/components/MSScreenView';

import { Footer } from './components/Footer';
import { Form } from './components/Form';
import { Welcome } from './components/Welcome';

export const SignUp: FC = () => {
    return (
        <MSScreenView
            bgColor='$gray200'
            px='$12'
            pb='$0'>
            <Welcome />

            <Form />

            <Footer />
        </MSScreenView>
    );
};
