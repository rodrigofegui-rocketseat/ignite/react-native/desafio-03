import { object, string, ref } from 'yup';

import {
    validateContainsNumber,
    validateMatchBRLPhone,
} from '@app/utils/validators';

export const signupSchema = object({
    name: string()
        .required('Informe o nome.')
        .test('has-number', 'Nome não pode conter números', (value) =>
            validateContainsNumber(value),
        ),
    email: string().required('Informe o e-mail.').email('E-mail inválido.'),
    phone: string()
        .required('Informe o telefone.')
        .test('is-brl-phone', 'Telefone inválido.', (value) =>
            validateMatchBRLPhone(value),
        ),
    password: string().required('Informe a senha.'),
    passwordConfirmation: string()
        .required('Informe a confirmação da senha.')
        .oneOf([ref('password'), ''], 'A confirmação da senha não confere.'),
});
