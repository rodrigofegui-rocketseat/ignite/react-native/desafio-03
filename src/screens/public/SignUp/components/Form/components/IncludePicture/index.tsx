import { View } from '@gluestack-ui/themed';
import { PencilSimpleLine } from 'phosphor-react-native';
import { FC } from 'react';

import { MSIconButton } from '@app/components/MSIconButton';
import { MSUserAvatar } from '@app/components/MSUserAvatar';

export const IncludePicture: FC = () => {
    return (
        <View mb='$3.5'>
            <MSUserAvatar.Root size='lg'>
                <MSUserAvatar.Image />
            </MSUserAvatar.Root>

            <View
                position='absolute'
                right='-$2'
                bottom='$0'>
                <MSIconButton
                    icon={PencilSimpleLine}
                    variant='primary'
                    size='md'
                />
            </View>
        </View>
    );
};
