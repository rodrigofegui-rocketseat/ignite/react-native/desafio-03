import { SignUpFormData } from './types';

export const handleSignUp = ({
    name,
    email,
    phone,
    password,
    passwordConfirmation,
}: SignUpFormData) => {
    console.debug('quis criar conta com:', {
        name,
        email,
        phone,
        password,
        passwordConfirmation,
    });
};
