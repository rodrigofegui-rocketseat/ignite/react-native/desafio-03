import { Box, Center } from '@gluestack-ui/themed';
import { yupResolver } from '@hookform/resolvers/yup';
import { FC, useRef } from 'react';
import { Controller, useForm } from 'react-hook-form';

import { MSButton } from '@app/components/MSButton';
import { MSInput } from '@app/components/MSInput';
import { SignableTextInputRef } from '@app/components/MSInput/TextInput/Root/types';
import { autoFocusOn } from '@app/utils/references';
import { brlPhoneMasker } from '@app/utils/validators/maskers';

import { IncludePicture } from './components/IncludePicture';
import { handleSignUp } from './controller';
import { signupSchema } from './schemas';
import { SignUpFormData } from './types';

export const Form: FC = () => {
    const emailRef = useRef<SignableTextInputRef | null>(null);
    const phoneRef = useRef<SignableTextInputRef | null>(null);
    const passwordRef = useRef<SignableTextInputRef | null>(null);
    const passwordConfirmationRef = useRef<SignableTextInputRef | null>(null);
    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm<SignUpFormData>({
        resolver: yupResolver(signupSchema),
    });

    return (
        <Center flex={1}>
            <IncludePicture />

            <Controller
                control={control}
                name='name'
                render={({ field: { onChange, value } }) => (
                    <MSInput.TextInput.Root
                        type='text'
                        placeholder='Nome'
                        returnKeyType='next'
                        value={value}
                        onChangeText={onChange}
                        errorMessage={errors.name?.message}
                        onSubmitEditing={() =>
                            autoFocusOn<SignableTextInputRef>(emailRef)
                        }
                    />
                )}
            />

            <Controller
                control={control}
                name='email'
                render={({ field: { onChange, value } }) => (
                    <MSInput.TextInput.Root
                        ref={emailRef}
                        type='text'
                        placeholder='E-mail'
                        keyboardType='email-address'
                        returnKeyType='next'
                        value={value}
                        onChangeText={onChange}
                        errorMessage={errors.email?.message}
                        onSubmitEditing={() =>
                            autoFocusOn<SignableTextInputRef>(phoneRef)
                        }
                    />
                )}
            />

            <Controller
                control={control}
                name='phone'
                render={({ field: { onChange, value } }) => (
                    <MSInput.TextInput.Root
                        ref={phoneRef}
                        type='text'
                        placeholder='Telefone'
                        returnKeyType='next'
                        keyboardType='phone-pad'
                        value={value}
                        masker={brlPhoneMasker}
                        onChangeText={onChange}
                        errorMessage={errors.phone?.message}
                        onSubmitEditing={() =>
                            autoFocusOn<SignableTextInputRef>(passwordRef)
                        }
                    />
                )}
            />

            <Controller
                control={control}
                name='password'
                render={({ field: { onChange, value } }) => (
                    <MSInput.TextInput.Root
                        ref={passwordRef}
                        type='password'
                        placeholder='Senha'
                        returnKeyType='next'
                        value={value}
                        onChangeText={onChange}
                        onSubmitEditing={() =>
                            autoFocusOn<SignableTextInputRef>(
                                passwordConfirmationRef,
                            )
                        }
                        errorMessage={errors.password?.message}
                    />
                )}
            />

            <Controller
                control={control}
                name='passwordConfirmation'
                render={({ field: { onChange, value } }) => (
                    <MSInput.TextInput.Root
                        ref={passwordConfirmationRef}
                        type='password'
                        placeholder='Confirmar senha'
                        returnKeyType='done'
                        isLast
                        value={value}
                        onChangeText={onChange}
                        onSubmitEditing={handleSubmit(handleSignUp)}
                        errorMessage={errors.passwordConfirmation?.message}
                    />
                )}
            />

            <Box
                mt='$8'
                w='$full'>
                <MSButton.Root
                    variant='dark'
                    onPress={handleSubmit(handleSignUp)}>
                    <MSButton.Text>Criar</MSButton.Text>
                </MSButton.Root>
            </Box>
        </Center>
    );
};
