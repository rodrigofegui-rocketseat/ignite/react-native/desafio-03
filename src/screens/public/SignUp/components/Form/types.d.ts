export type SignUpFormData = {
    name: string;
    email: string;
    phone: string;
    password: string;
    passwordConfirmation: string;
};
