import { Center } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSLogo } from '@app/components/MSLogo';
import { MSText } from '@app/components/MSText';

export const Welcome: FC = () => {
    return (
        <Center
            mt='$12'
            mb='$8'>
            <MSLogo
                size='xs'
                mb='$4'
            />
            <MSText
                tipo='título'
                fontSize='$xl'
                mb='$3'>
                Boas vindas!
            </MSText>
            <MSText
                tipo='subtítulo'
                fontSize='$sm'
                textAlign='center'>
                Crie sua conta e use o espaço para comprar itens variados e
                vender seus produtos
            </MSText>
        </Center>
    );
};
