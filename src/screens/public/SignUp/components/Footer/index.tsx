import { Box, Center } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSButton } from '@app/components/MSButton';
import { MSText } from '@app/components/MSText';

export const Footer: FC = () => {
    return (
        <Center py='$12'>
            <MSText mb='$4'>Já tem uma conta?</MSText>

            <Box w='$full'>
                <MSButton.Root
                    variant='secondary'
                    onPress={() => console.debug('deseja ir para login')}>
                    <MSButton.Text>Ir para o login</MSButton.Text>
                </MSButton.Root>
            </Box>
        </Center>
    );
};
