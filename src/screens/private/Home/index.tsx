import { FC } from 'react';

import { MSScreenView } from '@app/components/MSScreenView';

import { BuyItems } from './components/BuyItems';
import { Header } from './components/Header';
import { MyAds } from './components/MyAds';

export const Home: FC = () => {
    return (
        <MSScreenView
            bgColor='$gray200'
            pb='$0'
            withScroll={false}>
            <Header />

            <MyAds />

            <BuyItems />
        </MSScreenView>
    );
};
