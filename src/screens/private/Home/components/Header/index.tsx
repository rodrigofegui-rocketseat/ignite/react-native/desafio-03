import { HStack } from '@gluestack-ui/themed';
import { Plus } from 'phosphor-react-native';
import { FC, useMemo } from 'react';

import { MSButton } from '@app/components/MSButton';
import { MSUserAvatar } from '@app/components/MSUserAvatar';

export const Header: FC = () => {
    const mockedUser = useMemo(
        () => ({
            name: 'Fulado de Tal',
            imagemURL:
                'https://images.unsplash.com/photo-1579105728744-9d6b14a45389?q=80&w=44&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
        }),
        [],
    );

    return (
        <HStack>
            <MSUserAvatar.Root w='$3/5'>
                <MSUserAvatar.Image user={mockedUser} />
                <MSUserAvatar.FstLine>Boas vindas,</MSUserAvatar.FstLine>
                <MSUserAvatar.SndLine>
                    {mockedUser.name.split(' ')[0]}!
                </MSUserAvatar.SndLine>
            </MSUserAvatar.Root>

            <MSButton.Root variant='primary'>
                <MSButton.Icon as={Plus} />
                <MSButton.Text>Criar anúncio</MSButton.Text>
            </MSButton.Root>
        </HStack>
    );
};
