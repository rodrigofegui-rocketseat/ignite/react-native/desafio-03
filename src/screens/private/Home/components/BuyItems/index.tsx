import { VStack } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSAdvertisementList } from '../../../../../components/MSAdvertisementList';
import { Search } from '../Search';
import { SectionTitle } from '../SectionTitle';

export const BuyItems: FC = () => {
    return (
        <VStack
            mt='$8'
            flex={1}>
            <SectionTitle title='Compre produtos variados' />

            <Search />

            <MSAdvertisementList />
        </VStack>
    );
};
