import { Faders, MagnifyingGlass } from 'phosphor-react-native';
import { FC } from 'react';

import { MSInput } from '@app/components/MSInput';

export const Search: FC = () => {
    return (
        <MSInput.TextInput.Root
            type='text'
            placeholder='Buscar anúncio'
            returnKeyType='search'>
            <MSInput.TextInput.Action
                as={MagnifyingGlass}
                onPress={() => console.debug('outra ação MagnifyingGlass')}
            />
            <MSInput.TextInput.Action
                as={Faders}
                onPress={() => console.debug('outra ação Faders')}
            />
        </MSInput.TextInput.Root>
    );
};
