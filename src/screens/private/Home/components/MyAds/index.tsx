import { HStack, VStack } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSActionLine } from '@app/components/MSActionLine';
import { useColorTransparency } from '@app/hooks/useColorTransparency';

import { ActiveAdsCounter } from '../ActiveAdsCounter';
import { SectionTitle } from '../SectionTitle';

export const MyAds: FC = () => {
    const bgColor = useColorTransparency('primary400', 35);

    return (
        <VStack mt='$8'>
            <SectionTitle title='Seus produtos anunciados para venda' />

            <HStack
                bg={bgColor}
                px='$4.5'
                py='$3'
                rounded='$md'
                alignItems='center'>
                <ActiveAdsCounter />
                <MSActionLine title='Meus anúncios' />
            </HStack>
        </VStack>
    );
};
