import { FC } from 'react';

import { MSText } from '@app/components/MSText';

import { SectionTitleProps } from './types';

export const SectionTitle: FC<SectionTitleProps> = ({ title }) => {
    return (
        <MSText
            fontSize='$sm'
            color='$gray500'
            mb='$3'>
            {title}
        </MSText>
    );
};
