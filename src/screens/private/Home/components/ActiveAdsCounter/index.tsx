import { ButtonIcon, HStack, VStack } from '@gluestack-ui/themed';
import { Tag } from 'phosphor-react-native';
import { FC } from 'react';

import { MSText } from '@app/components/MSText';

export const ActiveAdsCounter: FC<object> = () => {
    return (
        <HStack
            alignItems='center'
            flex={1}>
            <ButtonIcon
                as={Tag}
                size='xl'
                color='$primary600'
                mr='$3'
            />
            <VStack>
                <MSText
                    tipo='título'
                    fontSize='$xl'
                    color='$gray600'>
                    4
                </MSText>
                <MSText
                    fontSize='$xs'
                    color='$gray600'>
                    anúncios ativos
                </MSText>
            </VStack>
        </HStack>
    );
};
