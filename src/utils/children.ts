import { Children, FC, ReactElement, ReactNode } from 'react';

export const getChildOfType = <T extends FC<any>>(
    children: ReactNode,
    targetType: T,
) => {
    return Children.toArray(children).find(
        (child) =>
            _isReactElement(child) &&
            child.type?.toString() === targetType.toString(),
    ) as ReactElement | undefined;
};

export const getChildrenOfType = <T extends FC<any>>(
    children: ReactNode,
    targetType: T,
) => {
    return Children.toArray(children).filter(
        (child) =>
            _isReactElement(child) &&
            child.type?.toString() === targetType.toString(),
    ) as ReactElement[] | undefined;
};

const _isReactElement = (targetElement: any): targetElement is ReactElement => {
    return targetElement.hasOwnProperty('type');
};
