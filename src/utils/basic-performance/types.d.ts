export type BasicPerformanceFuncParams = {
    func: (...args: any) => any;
    delay?: number;
};
