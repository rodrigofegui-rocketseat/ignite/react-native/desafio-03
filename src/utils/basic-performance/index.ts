import { DEFAULT_DELAY } from './constants';
import { BasicPerformanceFuncParams } from './types';

export const debounce = ({
    func,
    delay = DEFAULT_DELAY,
}: BasicPerformanceFuncParams) => {
    let timeoutId: NodeJS.Timeout;

    return (...args: Parameters<typeof func>) => {
        clearTimeout(timeoutId);

        timeoutId = setTimeout(() => {
            func.apply(this, args);
        }, delay);
    };
};

export const throttle = ({
    func,
    delay = DEFAULT_DELAY,
}: BasicPerformanceFuncParams) => {
    let throttling = false;

    return (...args: Parameters<typeof func>) => {
        if (!throttling) {
            throttling = true;

            func(...args);

            setTimeout(() => {
                throttling = false;
            }, delay);
        }
    };
};
