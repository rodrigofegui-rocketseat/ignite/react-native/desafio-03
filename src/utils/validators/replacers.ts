export const brlPhoneReplacer = (
    matches: any,
    ddd: string,
    inicio: string,
    fim: string,
): string => {
    if (!ddd && !inicio && !fim) return '';

    if (!inicio && !fim) return `(${ddd})`;

    if (!fim) {
        if (inicio.length === 4) {
            return `(${ddd}) ${inicio}`;
        }

        return `(${ddd}) ${inicio.substring(0, 4)}-${inicio.substring(4)}`;
    }

    return `(${ddd}) ${inicio}-${fim}`;
};
