export const SHORT_BRL_PHONE_REGEX =
    /^(?<ddd>\d{0,2})(?<inicio>\d{0,4})(?<final>\d{0,4}).*/gm;

export const BRL_PHONE_REGEX =
    /^(?<ddd>\d{0,2})(?<inicio>\d{0,5})(?<final>\d{0,4}).*/gm;

export const FORMATTED_BRL_PHONE_REGEX =
    /^\((?<ddd>\d{2})\) (?<inicio>\d{4,5})-(?<final>\d{4})$/gm;
