import { FORMATTED_BRL_PHONE_REGEX } from './regex';

export const validateContainsNumber = (rawValue: string) => {
    return !rawValue.match(/\d/gm);
};

export const validateMatchBRLPhone = (rawValue: string) => {
    return !!rawValue.match(FORMATTED_BRL_PHONE_REGEX);
};
