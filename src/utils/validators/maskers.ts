import { BRL_PHONE_REGEX, SHORT_BRL_PHONE_REGEX } from './regex';
import { brlPhoneReplacer } from './replacers';

export const brlPhoneMasker = (rawPhone: string): string => {
    if (!rawPhone) return rawPhone;

    const justDigits = rawPhone.replace(/[^\d]/gm, '');

    if (justDigits.length <= 2) return justDigits;

    if (justDigits.length <= 10)
        return justDigits.replace(SHORT_BRL_PHONE_REGEX, brlPhoneReplacer);

    return justDigits.replace(BRL_PHONE_REGEX, brlPhoneReplacer);
};
