import { ForwardedRef } from 'react';

export const autoFocusOn = <T>(targetRef: ForwardedRef<T | null>) => {
    targetRef?.current?.focus();
};
