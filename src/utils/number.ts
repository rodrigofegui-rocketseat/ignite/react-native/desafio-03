export const decimalToHex = (percentage: number) =>
    Number(percentage).toString(16).padStart(2, '0');

export const toBRLPrice = (rawPrice: number) => {
    const [value, decimal] = Number(rawPrice)
        .toLocaleString('pt-BR')
        .split(',');

    return `${value},${decimal?.padEnd(2, '0') || '00'}`;
};
