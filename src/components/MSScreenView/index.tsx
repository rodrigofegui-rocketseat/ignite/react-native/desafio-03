import { Box, SafeAreaView, ScrollView } from '@gluestack-ui/themed';
import { FC, PropsWithChildren } from 'react';

import { useInsets } from '@app/hooks/useInsets';

import { MSScreenViewProps } from './types';

export const MSScreenView: FC<PropsWithChildren<MSScreenViewProps>> = ({
    px,
    py,
    pt,
    pb,
    pl,
    pr,
    children,
    bgColor = '$gray200',
    withScroll = true,
}) => {
    const { top, left, right, bottom } = useInsets();

    if (withScroll) {
        return (
            <ScrollView
                contentContainerStyle={{ flexGrow: 1 }}
                showsVerticalScrollIndicator={false}>
                <SafeAreaView
                    pt={pt ?? py ?? top}
                    pb={pb ?? py ?? bottom}
                    pl={pl ?? px ?? left}
                    pr={pr ?? px ?? right}
                    flexGrow={1}
                    bgColor={bgColor}>
                    {children}
                </SafeAreaView>
            </ScrollView>
        );
    }

    return (
        <Box flex={1}>
            <SafeAreaView
                pt={pt ?? py ?? top}
                pb={pb ?? py ?? bottom}
                pl={pl ?? px ?? left}
                pr={pr ?? px ?? right}
                flexGrow={1}
                bgColor={bgColor}>
                {children}
            </SafeAreaView>
        </Box>
    );
};
