import { SafeAreaView } from '@gluestack-ui/themed';
import { ComponentProps } from 'react';

export type MSScreenViewProps = Pick<
    ComponentProps<typeof SafeAreaView>,
    'bgColor' | 'px' | 'py' | 'pt' | 'pb' | 'pl' | 'pr'
> & {
    withScroll?: boolean;
};
