import { IconProps } from 'phosphor-react-native';

export type MSActionLineProps = {
    title: string;
    as?: FC<IconProps>;
};
