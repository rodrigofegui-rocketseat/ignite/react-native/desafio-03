import { HStack, ButtonIcon } from '@gluestack-ui/themed';
import { ArrowRight } from 'phosphor-react-native';
import { FC } from 'react';

import { MSText } from '../MSText';

import { MSActionLineProps } from './types';

export const MSActionLine: FC<MSActionLineProps> = ({ title }) => {
    return (
        <HStack>
            <MSText
                tipo='título'
                fontSize='$xs'
                color='$primary600'>
                {title}
            </MSText>
            <ButtonIcon
                ml='$2'
                size='sm'
                color='$primary600'
                as={ArrowRight}
            />
        </HStack>
    );
};
