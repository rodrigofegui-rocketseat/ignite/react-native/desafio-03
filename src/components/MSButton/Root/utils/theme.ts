import { Colors } from '@app/types/theme';

import { MSButtonProps } from '../../types';

export const getBackgroundColor = ({
    variant,
}: Pick<MSButtonProps, 'variant'>): Colors => {
    if (variant === 'primary') return 'primary400';

    if (variant === 'dark') return 'gray700';

    return 'gray300';
};

export const getActiveBackgroundColor = ({
    variant,
}: Pick<MSButtonProps, 'variant'>): Colors => {
    if (variant === 'primary') return 'primary600';

    if (variant === 'dark') return 'gray500';

    return 'gray400';
};

export const getContentColor = ({
    variant,
}: Pick<MSButtonProps, 'variant'>): Colors => {
    if (variant === 'secondary') return 'gray700';

    return 'gray100';
};
