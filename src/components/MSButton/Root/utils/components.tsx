import { cloneElement, ReactElement } from 'react';

import { Spinner } from '../../Spinner';

export const getIcon = (
    contentColor: string,
    isLoading?: boolean,
    iconChild?: ReactElement,
) => {
    if (isLoading) return <Spinner color={contentColor} />;

    if (!iconChild) return null;

    return cloneElement(iconChild, { color: contentColor });
};

export const getText = (contentColor: string, textChild?: ReactElement) => {
    if (!textChild) return null;

    return cloneElement(textChild, { color: contentColor });
};
