import { Button } from '@gluestack-ui/themed';
import { ComponentProps } from 'react';

export type MSButtonRootProps = Omit<
    ComponentProps<typeof Button>,
    'variant'
> & {
    variant?: 'primary' | 'dark' | 'secondary';
    isLoading?: boolean;
};
