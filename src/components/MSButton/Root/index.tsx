import { Button, useToken } from '@gluestack-ui/themed';
import { FC, PropsWithChildren } from 'react';

import { getChildOfType } from '@app/utils/children';

import { Icon } from '../Icon';
import { Text } from '../Text';

import { MSButtonRootProps } from './types';
import { getIcon, getText } from './utils/components';
import {
    getActiveBackgroundColor,
    getBackgroundColor,
    getContentColor,
} from './utils/theme';

export const Root: FC<PropsWithChildren<MSButtonRootProps>> = ({
    children,
    onPress,
    variant = 'primary',
    isLoading = false,
    ...rest
}) => {
    const bgColor = useToken('colors', getBackgroundColor({ variant }));
    const activeBgColor = useToken(
        'colors',
        getActiveBackgroundColor({ variant }),
    );
    const contentColor = useToken('colors', getContentColor({ variant }));

    const iconChild = getChildOfType(children, Icon);
    const textChild = getChildOfType(children, Text);

    return (
        <Button
            variant='solid'
            action='primary'
            rounded='$lg'
            h='$10.5'
            flex={1}
            isFocusVisible={false}
            isDisabled={isLoading}
            onPress={onPress}
            bg={bgColor}
            $active-bg={activeBgColor}
            sx={{
                _text: {
                    color: contentColor,
                },
                _icon: {
                    color: contentColor,
                },
                _spinner: {
                    color: contentColor,
                },
            }}
            {...rest}>
            {getIcon(contentColor, isLoading, iconChild)}
            {getText(contentColor, textChild)}
        </Button>
    );
};
