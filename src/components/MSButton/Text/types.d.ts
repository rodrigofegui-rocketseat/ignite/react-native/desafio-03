import { ButtonText } from '@gluestack-ui/themed';

export type MSButtontextProps = ComponentProps<typeof ButtonText>;

export type DefaultValuesProps = {
    fontSize: FontSizes;
};
