import { ButtonText, useToken } from '@gluestack-ui/themed';
import { FC, PropsWithChildren } from 'react';

import { useLineHeight } from '@app/hooks/useLineHeight';

import { DEFAULT_VALUES } from './data';
import { MSButtontextProps } from './types';

export const Text: FC<PropsWithChildren<MSButtontextProps>> = ({
    children,
    ...rest
}) => {
    const fontSizeToken = useToken('fontSizes', DEFAULT_VALUES.fontSize);
    const lineHeight = useLineHeight(DEFAULT_VALUES.fontSize);

    return (
        <ButtonText
            fontFamily='$heading'
            fontSize={fontSizeToken}
            lineHeight={lineHeight}
            {...rest}>
            {children}
        </ButtonText>
    );
};
