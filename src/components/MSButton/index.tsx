import { Icon } from './Icon';
import { Root } from './Root';
import { Text } from './Text';

export const MSButton = {
    Root,
    Text,
    Icon,
};
