import { ButtonSpinner } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSButtonSpinnerProps } from './types';

export const Spinner: FC<MSButtonSpinnerProps> = ({ ...rest }) => {
    return (
        <ButtonSpinner
            mr='$2.5'
            {...rest}
        />
    );
};
