import { ButtonSpinner } from '@gluestack-ui/themed';
import { ComponentProps } from 'react';

export type MSButtonSpinnerProps = ComponentProps<typeof ButtonSpinner>;
