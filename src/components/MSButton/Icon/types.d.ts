import { ButtonIcon } from '@gluestack-ui/themed';
import { IconProps } from 'phosphor-react-native';
import { ComponentProps, FC } from 'react';

export type MSButtonIconProps = ComponentProps<typeof ButtonIcon> &
    IconProps & {
        as: FC<IconProps>;
    };
