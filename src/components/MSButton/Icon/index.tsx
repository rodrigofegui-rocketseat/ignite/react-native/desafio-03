import { ButtonIcon } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSButtonIconProps } from './types';

export const Icon: FC<MSButtonIconProps> = ({ as, color, ...rest }) => {
    return (
        <ButtonIcon
            mr='$2.5'
            size='sm'
            as={as}
            color={color}
            {...rest}
        />
    );
};
