export type MSButtonProps = {
    title: string;
    variant?: 'primary' | 'dark' | 'secondary';
    icon?: ReactNode;
};
