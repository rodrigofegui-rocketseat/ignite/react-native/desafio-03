import {
    Avatar,
    AvatarFallbackText,
    AvatarImage,
    Icon,
    useToken,
} from '@gluestack-ui/themed';
import { User } from 'phosphor-react-native';
import { FC, useMemo } from 'react';

import { MSUserAvatarImageProps } from './types';
import { getPictureSizeToken } from './utils';

export const Image: FC<MSUserAvatarImageProps> = ({
    user,
    size = 'md',
    isHighlighted = true,
}) => {
    const { avatarSize, iconSize, avatarBorderWidth } = getPictureSizeToken({
        size,
    });
    const avatarBorderWidthToken = useToken('borderWidths', avatarBorderWidth);
    const avatarSizeToken = useToken('space', avatarSize);
    const iconSizeToken = useToken('space', iconSize);

    const username = useMemo(() => user?.name || 'anônimo', [user?.name]);
    const altImage = useMemo(
        () => `Foto perfil do usuário ${username}`,
        [username],
    );
    const source = useMemo(
        () => (user?.imagemURL ? { uri: user.imagemURL } : null),
        [user?.imagemURL],
    );

    return (
        <Avatar
            bg='$gray300'
            borderColor={isHighlighted ? '$primary400' : '$white'}
            w={avatarSizeToken}
            h={avatarSizeToken}
            borderWidth={avatarBorderWidthToken}>
            {source && <AvatarFallbackText>{username}</AvatarFallbackText>}
            {source && (
                <AvatarImage
                    source={source}
                    alt={altImage}
                />
            )}
            {!source && (
                <Icon
                    as={User}
                    fill='$gray400'
                    color='$gray400'
                    size={iconSizeToken}
                />
            )}
        </Avatar>
    );
};
