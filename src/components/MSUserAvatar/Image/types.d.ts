import { BorderWidths, Spaces } from '@app/types/theme';
import { UserData } from '@app/types/user';

export type MSUserAvatarImageProps = {
    size?: 'sm' | 'md' | 'lg';
    user?: UserData;
    isHighlighted?: boolean;
};

export type PictureSizeTokenReturn = {
    avatarSize: Spaces;
    iconSize: Spaces;
    avatarBorderWidth: BorderWidths;
};
