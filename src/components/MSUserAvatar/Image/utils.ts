import { MSUserAvatarImageProps, PictureSizeTokenReturn } from './types';

export const getPictureSizeToken = ({
    size,
}: Pick<MSUserAvatarImageProps, 'size'>): PictureSizeTokenReturn => {
    if (size === 'sm') {
        return {
            avatarSize: '6',
            iconSize: '3.5',
            avatarBorderWidth: '1',
        };
    }

    if (size === 'md') {
        return {
            avatarSize: '11',
            iconSize: '6',
            avatarBorderWidth: '2',
        };
    }

    return {
        avatarSize: '22',
        iconSize: '12',
        avatarBorderWidth: '3',
    };
};
