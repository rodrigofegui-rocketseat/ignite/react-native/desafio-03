import { FC, PropsWithChildren } from 'react';

import { MSText } from '@app/components/MSText';

export const FstLine: FC<PropsWithChildren<object>> = ({ children }) => {
    return <MSText>{children}</MSText>;
};
