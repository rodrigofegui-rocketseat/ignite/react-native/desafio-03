import { FstLine } from './FstLine';
import { Image } from './Image';
import { Root } from './Root';
import { SndLine } from './SndLine';

export const MSUserAvatar = {
    Root,
    FstLine,
    Image,
    SndLine,
};
