import { HStack } from '@gluestack-ui/themed';
import { ComponentType } from 'react';

export type MSUserAvatarRootProps = ComponentType<typeof HStack> & {
    size?: 'sm' | 'md' | 'lg';
};
