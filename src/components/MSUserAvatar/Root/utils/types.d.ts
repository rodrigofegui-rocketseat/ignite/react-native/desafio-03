import { ReactElement } from 'react';

import { Image } from '../../Image';
import { MSUserAvatarImageProps } from '../../Image/types';

export type GetImageReq = Pick<MSUserAvatarImageProps, 'size'> & {
    child?: ReactElement<Image>;
};

export type GetFstLineReq = {
    child?: ReactElement<FstLine>;
};

export type GetSndLineReq = {
    child?: ReactElement<SndLine>;
};
