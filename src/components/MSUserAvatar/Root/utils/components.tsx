import { cloneElement } from 'react';

import { GetFstLineReq, GetImageReq, GetSndLineReq } from './types';

export const getImage = ({ size, child }: GetImageReq) => {
    if (!child) return null;

    return cloneElement(child, { size });
};

export const getFstLine = ({ size, child }: GetFstLineReq) => {
    if (!child) return null;

    return cloneElement(child, { size });
};

export const getSndLine = ({ size, child }: GetSndLineReq) => {
    if (!child) return null;

    return cloneElement(child, { size });
};
