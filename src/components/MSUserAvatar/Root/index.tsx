import { HStack, VStack } from '@gluestack-ui/themed';
import { FC, PropsWithChildren } from 'react';

import { getChildOfType } from '@app/utils/children';

import { FstLine } from '../FstLine';
import { Image } from '../Image';
import { SndLine } from '../SndLine';

import { MSUserAvatarRootProps } from './types';
import { getFstLine, getImage, getSndLine } from './utils/components';

export const Root: FC<PropsWithChildren<MSUserAvatarRootProps>> = ({
    size = 'md',
    children,
    ...rest
}) => {
    const imageChild = getChildOfType(children, Image);
    const fstLineChild = getChildOfType(children, FstLine);
    const sndLineChild = getChildOfType(children, SndLine);

    return (
        <HStack
            space='md'
            {...rest}>
            {getImage({ child: imageChild, size })}
            {fstLineChild && sndLineChild ? (
                <VStack>
                    {getFstLine({ child: fstLineChild })}
                    {getSndLine({ child: sndLineChild })}
                </VStack>
            ) : null}
        </HStack>
    );
};
