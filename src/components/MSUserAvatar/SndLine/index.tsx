import { FC, PropsWithChildren } from 'react';

import { MSText } from '@app/components/MSText';

export const SndLine: FC<PropsWithChildren<object>> = ({ children }) => {
    return <MSText tipo='título'>{children}</MSText>;
};
