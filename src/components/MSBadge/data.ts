import { DefaultValuesProps } from './types';

export const DEFAULT_VALUES: DefaultValuesProps = {
    variant: 'primary',
    fontSize: '2xs',
};
