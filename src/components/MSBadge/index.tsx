import { Badge, BadgeText, useToken } from '@gluestack-ui/themed';
import { FC } from 'react';

import { useLineHeight } from '@app/hooks/useLineHeight';

import { DEFAULT_VALUES } from './data';
import { MSBadgeProps } from './types';

export const MSBadge: FC<MSBadgeProps> = ({
    variant = DEFAULT_VALUES.variant,
    label,
    ...rest
}) => {
    const fontSizeToken = useToken('fontSizes', DEFAULT_VALUES.fontSize);
    const lineHeight = useLineHeight(DEFAULT_VALUES.fontSize);

    return (
        <Badge
            bg={
                variant === 'dark'
                    ? '$gray600'
                    : variant === 'primary'
                      ? '$primary600'
                      : '$gray300'
            }
            variant='solid'
            rounded='$full'
            {...rest}>
            <BadgeText
                color='white'
                fontFamily='$heading'
                fontSize={fontSizeToken}
                lineHeight={lineHeight}>
                {label}
            </BadgeText>
        </Badge>
    );
};
