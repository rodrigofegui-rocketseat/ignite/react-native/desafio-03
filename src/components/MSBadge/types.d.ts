import { Badge } from '@gluestack-ui/themed';
import { ComponentProps } from 'react';

import { FontSizes } from '@app/types/theme';

export type MSBadgeProps = Omit<ComponentProps<typeof Badge>, 'variant'> & {
    variant: 'dark' | 'primary' | 'light';
    label: string;
};

export type DefaultValuesProps = {
    variant: keyof typeof Pick<MSBadgeProps, 'variant'>;
    fontSize: FontSizes;
};
