import { Box, FlatList, useToken } from '@gluestack-ui/themed';
import { FC } from 'react';

import { ActionListSeparator } from '../ActionListSeparator';

import { ActionListProps } from './types';
import { renderItem } from './utils';

export const ActionList: FC<ActionListProps> = ({ actions }) => {
    const paddingVertical = useToken('space', '3');

    return (
        <Box>
            <FlatList
                contentContainerStyle={{
                    paddingVertical,
                }}
                data={actions}
                renderItem={renderItem}
                ItemSeparatorComponent={ActionListSeparator}
                horizontal
                showsHorizontalScrollIndicator={false}
            />
        </Box>
    );
};
