import { cloneElement } from 'react';

// @ts-ignore To compatible with FlatList:renderItem props
export const renderItem = ({ item, index }) =>
    cloneElement(item, { key: index });
