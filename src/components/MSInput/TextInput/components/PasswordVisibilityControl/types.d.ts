import { TextInputProps } from '../../types';

export type PasswordVisibilityControlProps = {
    inputType: typeof Pick<TextInputProps, 'type'>;
    setState: Dispatch<SetStateAction<'text' | 'password'>>;
};
