import { InputSlot, InputIcon } from '@gluestack-ui/themed';
import { Eye, EyeSlash } from 'phosphor-react-native';
import { FC } from 'react';

import { PasswordVisibilityControlProps } from './types';

export const PasswordVisibilityControl: FC<PasswordVisibilityControlProps> = ({
    inputType,
    setState,
}) => {
    return (
        <InputSlot
            onPress={() =>
                setState((previousType: 'text' | 'password') =>
                    previousType === 'password' ? 'text' : 'password',
                )
            }>
            <InputIcon
                as={inputType === 'text' ? Eye : EyeSlash}
                color='$gray500'
                size='lg'
            />
        </InputSlot>
    );
};
