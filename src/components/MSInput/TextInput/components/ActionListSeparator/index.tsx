import { Box } from '@gluestack-ui/themed';
import { memo } from 'react';

export const ActionListSeparator = memo(() => (
    <Box
        w='$px'
        bg='$gray400'
        mx='$2'
    />
));
