import { InputSlot } from '@gluestack-ui/themed';

export type MSTextInputActionProps = Pick<
    ComponentProps<typeof InputSlot>,
    'onPress'
> &
    ComponentProps<typeof InputIcon>;
