import { InputIcon, InputSlot } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSTextInputActionProps } from './types';

export const MSTextInputAction: FC<MSTextInputActionProps> = ({
    onPress,
    ...rest
}) => {
    return (
        <InputSlot onPress={onPress}>
            <InputIcon
                size='lg'
                color='$darkBlue500'
                weight='bold'
                {...rest}
            />
        </InputSlot>
    );
};
