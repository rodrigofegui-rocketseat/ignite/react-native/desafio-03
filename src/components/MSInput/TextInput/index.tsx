import { MSTextInputAction } from './Action';
import { MSTextInputRoot } from './Root';

export const TextInput = {
    Root: MSTextInputRoot,
    Action: MSTextInputAction,
};
