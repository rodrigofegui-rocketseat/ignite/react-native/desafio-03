import { InputField } from '@gluestack-ui/themed';

export type SignableTextInputRef = StyledComponentProps<
    StyleProp<TextStyle>,
    unknown,
    TextInputProps,
    'InputField',
    typeof TextInput
>;

export type MSTextInputRootProps = ComponentProps<typeof InputField> & {
    errorMessage?: string;
    isInvalid?: boolean;
    masker?: (a: string) => string;
    isLast?: boolean;
};

export type DefaultValuesProps = {
    fontSize: FontSizes;
};
