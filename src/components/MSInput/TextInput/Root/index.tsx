import {
    Input,
    InputField,
    FormControl,
    useToken,
    FormControlError,
    FormControlErrorText,
} from '@gluestack-ui/themed';
import { useState, forwardRef, PropsWithChildren } from 'react';

import { useLineHeight } from '@app/hooks/useLineHeight';
import { getChildrenOfType } from '@app/utils/children';

import { MSTextInputAction } from '../Action';
import { ActionList } from '../components/ActionList';
import { PasswordVisibilityControl } from '../components/PasswordVisibilityControl';

import { DEFAULT_VALUES } from './data';
import { MSTextInputRootProps, SignableTextInputRef } from './types';

export const MSTextInputRoot = forwardRef<
    SignableTextInputRef,
    PropsWithChildren<MSTextInputRootProps>
>(
    (
        {
            type = 'text',
            errorMessage = null,
            isInvalid = false,
            masker = (a: any) => a,
            onChangeText,
            isLast = false,
            children,
            ...rest
        },
        inputFieldRef,
    ) => {
        const [inputType, setInputType] = useState(type);
        const colorGray500 = useToken('colors', 'gray500');
        const lineHeight = useLineHeight(DEFAULT_VALUES.fontSize);
        const actions = getChildrenOfType(children, MSTextInputAction);

        const invalid = !!errorMessage || isInvalid;

        console.debug({ actions });

        return (
            <FormControl
                isInvalid={invalid}
                w='$full'>
                <Input
                    variant='outline'
                    size={DEFAULT_VALUES.fontSize}
                    rounded='$md'
                    bg='$gray100'
                    h='$11'
                    mb={!isLast ? '$4' : '$0'}
                    px='$4'
                    borderWidth='$0'
                    isDisabled={false}
                    isInvalid={invalid}
                    $focus={{
                        borderWidth: '$1',
                        borderColor: '$gray500',
                    }}
                    $invalid={{
                        borderWidth: 1,
                        borderColor: '$red400',
                    }}>
                    <InputField
                        ref={inputFieldRef}
                        type={inputType}
                        flexGrow={1}
                        w='$5/6'
                        px='$0'
                        fontFamily='$body'
                        lineHeight={lineHeight}
                        color='$gray600'
                        cursorColor={colorGray500}
                        placeholderTextColor='$gray400'
                        onChangeText={(newValue) =>
                            onChangeText(masker(newValue))
                        }
                        {...rest}
                    />
                    {type === 'password' && (
                        <PasswordVisibilityControl
                            inputType={inputType}
                            setState={setInputType}
                        />
                    )}
                    {!!actions && <ActionList actions={actions} />}
                </Input>

                <FormControlError
                    mt={!isLast ? '-$4' : '$0'}
                    mb='$2'>
                    <FormControlErrorText color='$red400'>
                        {errorMessage}
                    </FormControlErrorText>
                </FormControlError>
            </FormControl>
        );
    },
);
