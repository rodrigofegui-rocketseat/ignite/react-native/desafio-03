import { DefaultValuesProps } from './types';

export const DEFAULT_VALUES: DefaultValuesProps = {
    fontSize: 'md',
};
