import { FlatList } from '@gluestack-ui/themed';
import { FC, useCallback, useMemo } from 'react';

import { MSAdvertisement } from '@app/components/MSAdvertisement';

export const MSAdvertisementList: FC = () => {
    const mockedUser = useMemo(
        () => ({
            name: 'Fulado de Tal',
            imagemURL:
                'https://images.unsplash.com/photo-1579105728744-9d6b14a45389?q=80&w=44&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
        }),
        [],
    );
    const advertisements = useMemo(
        () => [
            {
                id: '1',
                user: mockedUser,
                title: 'Tênis vermelho',
                imageUrls: [
                    'https://plus.unsplash.com/premium_photo-1665203630695-1db105e4ea9d?q=80&w=375&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ],
                price: 59.9,
                isNew: false,
                isActive: true,
            },
            {
                id: '2',
                user: mockedUser,
                title: 'Bicicleta',
                imageUrls: [
                    'https://plus.unsplash.com/premium_photo-1665203630695-1db105e4ea9d?q=80&w=375&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ],
                price: 120,
                isNew: true,
                isActive: true,
            },
            {
                id: '3',
                user: mockedUser,
                title: 'Tênis vermelho',
                imageUrls: [
                    'https://plus.unsplash.com/premium_photo-1665203630695-1db105e4ea9d?q=80&w=375&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ],
                price: 59.9,
                isNew: false,
                isActive: true,
            },
            {
                id: '4',
                user: mockedUser,
                title: 'Sofá 1,80m',
                imageUrls: [
                    'https://plus.unsplash.com/premium_photo-1665203630695-1db105e4ea9d?q=80&w=375&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ],
                price: 1200,
                isNew: true,
                isActive: true,
            },
            {
                id: '5',
                user: mockedUser,
                title: 'Coturno feminino',
                imageUrls: [
                    'https://plus.unsplash.com/premium_photo-1665203630695-1db105e4ea9d?q=80&w=375&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ],
                price: 80,
                isNew: false,
                isActive: true,
            },
            {
                id: '6',
                user: mockedUser,
                title: 'Roupa feminina',
                imageUrls: [
                    'https://plus.unsplash.com/premium_photo-1665203630695-1db105e4ea9d?q=80&w=375&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ],
                price: 59.9,
                isNew: false,
                isActive: true,
            },
            {
                id: '7',
                user: mockedUser,
                title: 'Luminária de mesa',
                imageUrls: [
                    'https://plus.unsplash.com/premium_photo-1665203630695-1db105e4ea9d?q=80&w=375&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ],
                price: 130,
                isNew: true,
                isActive: true,
            },
            {
                id: '8',
                user: mockedUser,
                title: 'Geladeira',
                imageUrls: [
                    'https://plus.unsplash.com/premium_photo-1665203630695-1db105e4ea9d?q=80&w=375&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ],
                price: 200,
                isNew: false,
                isActive: true,
            },
        ],
        [],
    );

    const renderItem = useCallback(
        ({ item }) => <MSAdvertisement item={item} />,
        [],
    );

    return (
        <FlatList
            data={advertisements}
            numColumns={2}
            columnWrapperStyle={{
                justifyContent: 'space-between',
            }}
            renderItem={renderItem}
        />
    );
};
