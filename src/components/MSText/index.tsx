import { Text } from '@gluestack-ui/themed';
import { FC, PropsWithChildren } from 'react';

import { useLineHeight } from '@app/hooks/useLineHeight';
import { FontSizes } from '@app/types/theme';

import { MSTextProps } from './types';
import { getFontFamily, getFontWeight } from './utils';

export const MSText: FC<PropsWithChildren<MSTextProps>> = ({
    tipo = 'texto',
    color = '$gray700',
    fontSize = '$md',
    children,
    ...rest
}) => {
    const lineHeight = useLineHeight(
        fontSize?.toString()?.substring(1) as FontSizes,
    );

    return (
        <Text
            {...rest}
            fontFamily={`$${getFontFamily({ tipo })}`}
            fontWeight={`$${getFontWeight({ tipo })}`}
            fontSize={fontSize}
            lineHeight={lineHeight}
            color={color}>
            {children}
        </Text>
    );
};
