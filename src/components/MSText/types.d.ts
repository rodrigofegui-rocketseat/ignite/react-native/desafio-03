import { Text } from '@gluestack-ui/themed';
import { ComponentProps } from 'react';

export type MSTextProps = ComponentProps<typeof Text> & {
    tipo?: 'texto' | 'título' | 'subtítulo';
};
