import { Fonts, FontWeights } from '@app/types/theme';

import { MSTextProps } from './types';

export const getFontFamily = ({ tipo }: MSTextProps): Fonts => {
    if (tipo === 'título') return 'heading';

    if (tipo === 'subtítulo') return 'light';

    return 'body';
};

export const getFontWeight = ({ tipo }: MSTextProps): FontWeights => {
    if (tipo === 'título') return 'bold';

    if (tipo === 'subtítulo') return 'light';

    return 'normal';
};
