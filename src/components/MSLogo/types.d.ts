import { Image } from '@gluestack-ui/themed';
import { ComponentProps } from 'react';

export type MSLogoProps = Pick<ComponentProps<typeof Image>, 'mb'> & {
    size?: 'xs' | 'lg';
};
