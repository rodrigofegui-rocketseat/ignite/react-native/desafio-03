import { Image } from '@gluestack-ui/themed';
import { FC } from 'react';

import LogoImg from '@assets/logo.png';

import { MSLogoProps } from './types';

export const MSLogo: FC<MSLogoProps> = ({ size = 'lg', ...rest }) => {
    return (
        <Image
            source={LogoImg}
            aspectRatio={1.5 /* original width / original height */}
            alt='Logitipo do Marketspace'
            rounded='$none'
            h={size === 'lg' ? '$20' : '$11'}
            {...rest}
        />
    );
};
