import { Box, HStack, Image, useToken } from '@gluestack-ui/themed';
import { FC, useMemo } from 'react';

import { MSBadge } from '@app/components/MSBadge';
import { MSUserAvatar } from '@app/components/MSUserAvatar';

import { MSAdvertisementVisualizationProps } from './types';

export const MSAdvertisementVisualization: FC<
    MSAdvertisementVisualizationProps
> = ({ item }) => {
    const imageHeight = useToken('space', '26.5');
    const paddingTop = useToken('space', '1');
    const imageSource = useMemo(
        () => ({ uri: item.imageUrls[0] }),
        [item.imageUrls],
    );
    const imagemAlt = useMemo(
        () => `Foto do produto ${item.title}`,
        [item.title],
    );

    return (
        <Box position='relative'>
            <Image
                h={imageHeight}
                aspectRatio={1.53}
                rounded='$md'
                source={imageSource}
                alt={imagemAlt}
            />

            <HStack
                zIndex={1}
                h='$5.5'
                bottom={imageHeight - paddingTop}
                px='$1'
                justifyContent='space-between'>
                <MSUserAvatar.Root size='sm'>
                    <MSUserAvatar.Image
                        user={item.user}
                        isHighlighted={false}
                    />
                </MSUserAvatar.Root>
                <MSBadge
                    variant={item.isNew ? 'primary' : 'dark'}
                    label={item.isNew ? 'novo' : 'usado'}
                />
            </HStack>
        </Box>
    );
};
