import { Advertisement } from '@app/types/advsertisement';

export type MSAdvertisementVisualizationProps = {
    item: Advertisement;
};
