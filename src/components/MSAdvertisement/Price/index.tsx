import { HStack } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSText } from '@app/components/MSText';
import { toBRLPrice } from '@app/utils/number';

import { MSAdvertisementPriceProps } from './types';

export const MSAdvertisementPrice: FC<MSAdvertisementPriceProps> = ({
    price,
}) => {
    return (
        <HStack alignItems='flex-end'>
            <MSText
                tipo='título'
                fontSize='$xs'>
                R${' '}
            </MSText>
            <MSText tipo='título'>{toBRLPrice(price)}</MSText>
        </HStack>
    );
};
