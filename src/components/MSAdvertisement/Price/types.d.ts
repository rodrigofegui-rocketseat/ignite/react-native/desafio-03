import { Advertisement } from '@app/types/advsertisement';

export type MSAdvertisementPriceProps = Pick<Advertisement, 'price'>;
