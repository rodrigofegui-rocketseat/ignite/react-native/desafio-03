import { VStack } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSAdvertisementPrice } from './Price';
import { MSAdvertisementRoot } from './Root';
import { MSAdvertisementTitle } from './Title';
import { MSAdvertisementProps } from './types';
import { MSAdvertisementVisualization } from './Visualization';

export const MSAdvertisement: FC<MSAdvertisementProps> = ({ item }) => {
    return (
        <MSAdvertisementRoot key={item.id}>
            <MSAdvertisementVisualization item={item} />
            <VStack mt='-$4'>
                <MSAdvertisementTitle title={item.title} />
                <MSAdvertisementPrice price={item.price} />
            </VStack>
        </MSAdvertisementRoot>
    );
};
