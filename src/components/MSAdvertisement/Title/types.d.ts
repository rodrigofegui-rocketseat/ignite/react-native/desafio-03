import { Advertisement } from '@app/types/advsertisement';

export type MSAdvertisementTitleProps = Pick<Advertisement, 'title'>;
