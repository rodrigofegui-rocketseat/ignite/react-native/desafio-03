import { memo } from 'react';

import { MSText } from '@app/components/MSText';

import { MSAdvertisementTitleProps } from './types';

export const MSAdvertisementTitle = memo<MSAdvertisementTitleProps>(
    ({ title }) => {
        return <MSText fontSize='$sm'>{title}</MSText>;
    },
);
