import { UserData } from '@app/types/user';

export type MSAdvertisementRootProps = {
    user: UserData;
    isNew: boolean;
};
