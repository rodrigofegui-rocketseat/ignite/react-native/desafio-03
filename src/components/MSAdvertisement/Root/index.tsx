import { VStack } from '@gluestack-ui/themed';
import { FC, PropsWithChildren } from 'react';

export const MSAdvertisementRoot: FC<PropsWithChildren<any>> = ({
    children,
}) => {
    return <VStack mb='$8'>{children}</VStack>;
};
