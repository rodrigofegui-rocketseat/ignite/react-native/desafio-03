import { Advertisement } from '@app/types/advsertisement';

export type MSAdvertisementProps = {
    item: Advertisement;
};
