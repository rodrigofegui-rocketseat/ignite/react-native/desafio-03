import { Button, ButtonIcon, useToken } from '@gluestack-ui/themed';
import { FC } from 'react';

import { MSIconButtonProps } from './types';
import { getColorTokens, getSizeTokens } from './utils';

export const MSIconButton: FC<MSIconButtonProps> = ({
    variant = 'primary',
    size = 'lg',
    icon: Icon,
    onPress,
    ...rest
}) => {
    const { buttonSize, iconSize } = getSizeTokens({ size });
    const { buttonBg, buttonActiveBg, iconColor } = getColorTokens({ variant });

    const buttonSizeToken = useToken('space', buttonSize);
    const iconSizeToken = useToken('space', iconSize);

    const buttonBgToken = useToken('colors', buttonBg);
    const buttonActiveBgToken = useToken('colors', buttonActiveBg);
    const iconColorToken = useToken('colors', iconColor);

    return (
        <Button
            borderRadius='$full'
            onPress={onPress}
            w={buttonSizeToken}
            h={buttonSizeToken}
            bg={buttonBgToken}
            borderColor={buttonBgToken}
            $active-bg={buttonActiveBgToken}
            sx={{
                _icon: {
                    color: iconColorToken,
                },
            }}
            {...rest}>
            <ButtonIcon
                as={Icon}
                color={iconColorToken}
                size={iconSizeToken}
            />
        </Button>
    );
};
