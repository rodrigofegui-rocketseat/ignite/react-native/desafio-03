import { Button } from '@gluestack-ui/themed';
import { IconProps } from 'phosphor-react-native';
import { ComponentProps } from 'react';

import { Spaces } from '@app/types/theme';

export type MSIconButtonProps = Omit<
    ComponentProps<typeof Button>,
    'variant' | 'size'
> & {
    variant?: 'primary' | 'light';
    size?: 'md' | 'lg';
    icon: FC<IconProps>;
};

export type SizeTokensReq = Required<Pick<MSIconButtonProps, 'size'>>;
export type ColorTokensReq = Required<Pick<MSIconButtonProps, 'variant'>>;

export type SizeTokensRes = {
    buttonSize: Spaces;
    iconSize: Spaces;
};

export type ColorTokensRes = {
    buttonBg: Colors;
    buttonActiveBg: Colors;
    iconColor: Colors;
};
