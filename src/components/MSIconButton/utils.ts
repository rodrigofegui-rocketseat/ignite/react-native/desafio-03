import {
    ColorTokensReq,
    ColorTokensRes,
    SizeTokensReq,
    SizeTokensRes,
} from './types';

export const getSizeTokens = ({ size }: SizeTokensReq): SizeTokensRes => {
    if (size === 'md') {
        return {
            buttonSize: '10',
            iconSize: '4',
        };
    }

    return {
        buttonSize: '12',
        iconSize: '6',
    };
};

export const getColorTokens = ({ variant }: ColorTokensReq): ColorTokensRes => {
    if (variant === 'primary') {
        return {
            buttonBg: 'primary400',
            buttonActiveBg: 'primary600',
            iconColor: 'white',
        };
    }

    return {
        buttonBg: 'transparent',
        buttonActiveBg: 'gray300',
        iconColor: 'gray700',
    };
};
