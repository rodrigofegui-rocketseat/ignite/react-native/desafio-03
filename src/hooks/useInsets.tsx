import { useToken } from '@gluestack-style/react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export const useInsets = () => {
    const { top, bottom, left, right } = useSafeAreaInsets();

    const tokenMinMarginBottom = useToken('space', '8');
    const tokenMarginTop = useToken('space', '10');
    const tokenMarginX = useToken('space', '6');

    return {
        top: top + tokenMarginTop,
        bottom: bottom + tokenMinMarginBottom,
        left: left + tokenMarginX,
        right: right + tokenMarginX,
    };
};
