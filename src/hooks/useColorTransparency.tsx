import { useToken } from '@gluestack-ui/themed';
import { useMemo } from 'react';

import { Colors } from '@app/types/theme';
import { decimalToHex } from '@app/utils/number';

export const useColorTransparency = (
    targetColor: Colors,
    percentage: number,
) => {
    const targetColorToken = useToken('colors', targetColor);
    const alphaHex = useMemo(() => decimalToHex(percentage), [percentage]);

    return `${targetColorToken}${alphaHex}`;
};
