import { useToken } from '@gluestack-style/react';

import { FontSizes } from '@app/types/theme';

export const useLineHeight = (fontSize: FontSizes): number => {
    const cFontSize = useToken('fontSizes', fontSize);

    const cLineHeight = useToken('lineHeights', '130%');

    return cLineHeight * cFontSize;
};
