import {
    Karla_400Regular,
    Karla_700Bold,
    Karla_300Light,
    useFonts,
} from '@expo-google-fonts/karla';
import { GluestackUIProvider, StatusBar } from '@gluestack-ui/themed';
import * as SplashScreen from 'expo-splash-screen';
import { FC, useCallback } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import { SignUp } from '@app/screens/publics/SignUp';

import { config } from '@config/gluestack-ui.config';

SplashScreen.preventAutoHideAsync();

const App: FC = () => {
    const [isFontsLoaded] = useFonts({
        Karla_300Light,
        Karla_400Regular,
        Karla_700Bold,
    });

    const hideSplashscreenWhenFonts = useCallback(async () => {
        if (isFontsLoaded) {
            await SplashScreen.hideAsync();
        }
    }, [isFontsLoaded]);

    if (!isFontsLoaded) {
        return null;
    }

    return (
        <GluestackUIProvider config={config}>
            <SafeAreaProvider onLayout={hideSplashscreenWhenFonts}>
                <StatusBar
                    barStyle='dark-content'
                    backgroundColor='transparent'
                    translucent
                />
                <SignUp />
            </SafeAreaProvider>
        </GluestackUIProvider>
    );
};

export default App;
